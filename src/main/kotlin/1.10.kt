/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.10 Escriu un marc per un String
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.next()
    val input2 = scanner.next().single()
    println(enmarca(input1,input2))
}

fun enmarca(paraula:String, simbol:Char){
    for (i in 1..5) {
        for (j in 1..paraula.length+2) {
            if (i==1 || i==5 || j==1 || j==paraula.length+2) print(simbol)
            else if (i ==3 && j==2) print(paraula)
            else if (i!=3) print(" ")
        }
        println()
    }
}
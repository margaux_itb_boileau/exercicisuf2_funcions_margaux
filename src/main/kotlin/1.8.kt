/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.8 Escriu una línia
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.nextInt()
    val input2 = scanner.nextInt()
    val input3 = scanner.next().single()
    println(escriu(input1,input2,input3))
}

fun escriu(espais:Int, nombreLletres:Int, lletra:Char):String{
    var linia = ""
    for (s in 1..espais) linia+=" "
    for (n in 1..nombreLletres) linia+=lletra
    return linia
}
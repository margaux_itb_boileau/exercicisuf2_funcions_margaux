/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.6 Divideix un String
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.next()
    val input2 = scanner.next().single()
    println(divideix(input1,input2))
}

fun divideix(paraula:String, lletra:Char):MutableList<String> {
    var str = ""
    val mut = mutableListOf<String>()
    for (i in paraula) {
        if (i != lletra) {
            str+=i
        }
        else {
            mut.add(str)
            str=""
            continue
        }
    }
    return mut
}
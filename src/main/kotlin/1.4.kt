/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.4 Subseqüència
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.next()
    val input2 = scanner.nextInt()
    val input3 = scanner.nextInt()
    println(subsequencia(input1,input2,input3))
}

fun subsequencia(paraula:String, inici:Int, final:Int) {
    if ((inici..final).contains(paraula.lastIndex)) {
        println("La subseqüència $inici - $final de l'String no existeix")
    }
    else{
        for (i in inici..final) print(paraula[i])
    }
}
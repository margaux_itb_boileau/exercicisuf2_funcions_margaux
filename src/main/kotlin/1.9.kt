/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.9 Escriu una creu
*/
import kotlin.math.roundToInt
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.nextInt()
    val input2 = scanner.next().single()
    println(creu(input1,input2))
}

fun creu(n:Int, simbol:Char){
    if (n>=3) {
        for (i in 1..n) {
            for (j in 1..n) {
                if (j==(n/2.0).roundToInt() || i==(n/2.0).roundToInt()) print(simbol)
                else print(" ")
            }
            println()
        }
    }
}
/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.1 Mida d’un String
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.next()
    println(mida(input1))
}

fun mida(paraula:String):Int {
    var mida = 0
    for (i in paraula) mida++
    return mida
}
/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.7 Eleva’l
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.nextInt()
    val input2 = scanner.nextInt()
    println(eleva(input1,input2))
}

fun eleva(base:Int, exponent:Int):Long{
    val result:Long
    when (exponent) {
        0 -> result=1
        1 -> result=base.toLong()
        2 -> result=base*base.toLong()
        else -> result=base*eleva(base,exponent-1)
    }
    return(result)
}

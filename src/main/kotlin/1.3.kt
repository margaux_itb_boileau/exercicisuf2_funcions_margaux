/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.3 Caràcter d’un String
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.next()
    val input2 = scanner.nextInt()
    println(retornaElChar(input1,input2))
}

fun retornaElChar(paraula:String, posicio:Int) {
    if (posicio < paraula.lastIndex) println("La mida de l'String és inferior a 10")
    else if (posicio > paraula.lastIndex) println("La mida de l'String és major a 10")
    else println(paraula[posicio])
}
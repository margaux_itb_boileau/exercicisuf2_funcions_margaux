/*
* AUTHOR: Margaux Boileau
* DATE: 2022/12/17
* TITLE: 1.5 D’String a MutableList<Char>
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val input1 = scanner.next()
    println(toMutListChar(input1))
}

fun toMutListChar(paraula:String):MutableList<Char> {
    val mut = mutableListOf<Char>()
    for (i in paraula) mut.add(i)
    return mut
}